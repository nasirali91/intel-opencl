<!DOCTYPE html>
<!--
Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

This agreement shall be governed in all respects by the laws of the State of California and
by the laws of the United States of America.
-->
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>Fast Fourier Transform (1D) using Off-Chip Memory: Intel FPGA&reg; OpenCL&trade; Design Example</title>
<link rel="stylesheet" href="../common/readme.css" type="text/css">
</head>
<body>
<h1>
<div class="preheading">Intel FPGA<sup>&reg;</sup> OpenCL&trade; Design Example</div>
Fast Fourier Transform (1D) using Off-Chip Memory
</h1>

<p>This readme file for the Fast Fourier Transform (1D) using Off-Chip Memory OpenCL Design Example contains
information about the design example package. For more examples, please
visit the <a href="https://www.altera.com/products/design-software/embedded-software-developers/opencl/developer-zone.html">
Intel FPGA OpenCL Design Examples page</a>.</p>
<nav>
<h2>Contents</h2>
<ul>
<li><a href="#Description">Description</a></li>
<li><a href="#Software_Hardware_Requirements">Software &amp; Hardware Requirements</a></li>
<li><a href="#Package_Contents">Package Contents</a></li>
<li><a href="#Compiling_the_OpenCL_Kernel">Compiling the OpenCL Kernel</a></li>
<li><a href="#Compiling_the_Host_Program">Compiling the Host Program</a></li>
<li><a href="#Running_the_Host_Program">Running the Host Program</a></li>
<li><a href="#Release_History">Release History</a></li>
<li><a href="#Legal">Legal</a></li>
<li><a href="#Contacting_Intel">Contacting Intel</a></li>
</ul>
</nav>
<section>
<a id="Description"><h2>Description</h2></a>
<p>This benchmark demonstrates an OpenCL implementation of a 1D Fast Fourier
      Transform (1D FFT) on Intel FPGAs. The benchmark can process up to 16 Million
      complex single-precision floating point values and supports dynamically changing
      data size. The algorithm used to process such large data sets has six stages.
      For example, assume we want to process 1 Million points:</p><ol>
        <li>Treating 1M points as 1K x 1K matrix, read it from external memory and
        transpose it on the fly.</li>
        <li>Run 1K 1D FFT on all the rows (of transposed matrix).</li>
        <li>Multiply resulting values by adjustment twiddle factors.</li>
        <li>Transpose the matrix and write to temporary buffer in external memory.</li>
        <li>Run 1K 1D FFT on all the rows.</li>
        <li>Transpose the matrix and write output to external memory.</li>
      </ol><p>The whole system consists of three kernels connected by channels (does not count
      two additional multi-wire transpose kernels whose only purpose is to transpose
      data each clock cycle). The set
      of three kernels is enqueued twice by the host to do the full computaion.
      First enqueue performs steps 1-4 above, the second enqueue does steps
      5-6. This is essentially a 2D FFT core with extra transposition and twiddle
      multiplication.</p><p>The responsibilities of the kernels are:</p><ol>
        <li><span class="mono">fetch()</span>:
        Reads data from external memory, transposes it for step 1, sends
        into kernel-to-kernel channels to fft1d() kernel.</li>
        <li><span class="mono">fft1d()</span>:
        Reads data from channels, performs 1D 1K transform on the rows,
        writes data to channels.</li>
        <li><span class="mono">transpose()</span>:
        Reads data from kernel channels, optionally multiplies by adjustment
        twiddle factors, transposes the data, and writes to external memory.</li>
      </ol><h3>Parameterization</h3><p>The design is easily parametrized as follows:</p><ul>
        <li><span class="mono">LOGN</span>: <span class="mono">log(total number of points in a col) / 2</span>, so total number
        of points processed by the core is <span class="mono">2<sup>2 × LOGN</sup></span>. So setting <span class="mono">LOGN</span>
        to 11 will give <span class="mono">2<sup>22</sup> = 4 million</span> point FFT core. Maximum supported
        value of LOGN is 12 (16 million points).
        This value is set in <span class="mono">host/inc/fft_config.h</span>.
        </li>

        <li><span class="mono">LOGM</span>: <span class="mono">log(total number of points in a row) / 2</span>, so total number
        of points processed by the core is <span class="mono">2<sup>2 × LOGN</sup></span>. So setting <span class="mono">LOGN</span>
        to 11 will give <span class="mono">2<sup>22</sup> = 4 million</span> point FFT core. Maximum supported
        value of LOGM is 12 (16 million points).
        This value is set in <span class="mono">host/inc/fft_config.h</span>.
        </li>        
        
        <li><span class="mono">LOGPOINTS</span>: <span class="mono">log(number of points to process in parallel)</span>.
        Valid values are 2 (4 points in parallel) and 3 (8 points in
        parallel).
        This value is set in <span class="mono">host/inc/fft_config.h</span>.
        </li>
         
        <li><span class="mono">inverse</span>: dynamic argument to the design, set during
        kernel invocation by the host. If set to 1, the core computes inverse FFT.</li>
        
        <li><span class="mono">mangle</span>: dynamic argument to the design, set during
        kernel invocation by the host. If set to 1, the kernel will use optimized memory 
        layout for input and output data. The host must match the data layout.</li>
      </ul>
</section>

<section>
<a id="Software_Hardware_Requirements"><h2>Software &amp; Hardware Requirements</h2></a>
<p/>
<table class="reqs">
<thead>
<tr>
  <th rowspan="3">Requirement</th>
  <th rowspan="3">Version</th>
<th colspan="4">OpenCL Kernel</th><th colspan="8">Host Program</th></tr><tr><th rowspan="2">Hardware<br/>Compile</th><th rowspan="2">Emulation<br/>Compile</th><th rowspan="2">Fast Emulation<br/>Compile</th><th rowspan="2">Simulation<br/>Compile</th><th colspan="2">Hardware</th><th colspan="2">Emulation</th><th colspan="2">Fast Emulation</th><th colspan="2">Simulation</th></tr><tr><th>Compile</th><th>Run</th><th>Compile</th><th>Run</th><th>Compile</th><th>Run</th><th>Compile</th><th>Run</th></tr></thead><tbody><tr><td>Quartus Prime Design Software <small>(Quartus II)</small></td><td>19.1 or later</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td>Intel(R) FPGA SDK for OpenCL(TM)</td><td>19.1 or later</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req" rowspan="2">&#x02713;<div class="either">(either)</div></td><td class="req" rowspan="2">&#x02713;<div class="either">(either)</div></td><td class="req" rowspan="2">&#x02713;<div class="either">(either)</div></td><td class="req" rowspan="2">&#x02713;<div class="either">(either)</div></td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req" rowspan="2">&#x02713;<div class="either">(either)</div></td><td class="req" rowspan="2">&#x02713;<div class="either">(either)</div></td></tr><tr><td>Intel(R) FPGA Runtime Environment for OpenCL(TM)</td><td>19.1 or later</td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td>Board Support Package</td><td>19.1-compatible</td><td class="req">&#x02713;</td><td></td><td></td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td>Board Hardware</td><td>-</td><td></td><td></td><td></td><td></td><td></td><td class="req">&#x02713;</td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><td>gcc</td><td>4.4.7 or later</td><td></td><td></td><td></td><td></td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td><td class="req">&#x02713;</td></tr><tr><td>GNU Make</td><td>3.8.1 or later</td><td></td><td></td><td></td><td></td><td class="req">&#x02713;</td><td></td><td class="req">&#x02713;</td><td></td><td class="req">&#x02713;</td><td></td><td class="req">&#x02713;</td><td></td></tr></tbody>
</table>

</section>

<section>
<a id="Package_Contents"><h2>Package Contents</h2></a>
<p/>
<table class="pkg-contents">
<thead>
<tr>
  <th class="path">Path</th>
  <th class="desc">Description</th>
</tr>
</thead>
<tbody>
<tr>
  <td class="path"><a href="./" style="padding-left: 0.0ex">fft1d_offchip/</a></td>
  <td class="desc"></td>
</tr>
<tr>
  <td class="path"><a href="./Makefile" style="padding-left: 2.0ex">Makefile</a></td>
  <td class="desc">Makefile for host program</td>
</tr>
<tr>
  <td class="path"><a href="./bin/" style="padding-left: 2.0ex">bin/</a></td>
  <td class="desc">Host program, AOCX files</td>
</tr>
<tr>
  <td class="path"><a href="./device/" style="padding-left: 2.0ex">device/</a></td>
  <td class="desc">OpenCL kernel files</td>
</tr>
<tr>
  <td class="path"><a href="./device/fft1d_offchip.cl" style="padding-left: 4.0ex">fft1d_offchip.cl</a></td>
  <td class="desc">Top-level OpenCL kernel file</td>
</tr>
<tr>
  <td class="path"><a href="./device/genscript/" style="padding-left: 4.0ex">genscript/</a></td>
  <td class="desc">Host data files</td>
</tr>
<tr>
  <td class="path"><a href="./host/" style="padding-left: 2.0ex">host/</a></td>
  <td class="desc"></td>
</tr>
<tr>
  <td class="path"><a href="./host/inc/" style="padding-left: 4.0ex">inc/</a></td>
  <td class="desc">Host include files</td>
</tr>
<tr>
  <td class="path"><a href="./host/src/" style="padding-left: 4.0ex">src/</a></td>
  <td class="desc">Host source files</td>
</tr>
</tbody>
</table>

</section>

<section>
<a id="Compiling_the_OpenCL_Kernel"><h2>Compiling the OpenCL Kernel</h2></a>
    <p>The top-level OpenCL kernel file is <span class="mono">device/fft1d_offchip.cl</span>.</p>
    <p>To compile the OpenCL kernel, run:</p>
    <div class="command">aoc device/fft1d_offchip.cl <span class="nowrap">-o</span> bin/fft1d_offchip.aocx<span class="nowrap"></span> <span class="nowrap">-fp-relaxed</span> -board=<span class="highlight">&lt;<i>board</i>&gt;</span></div>
    <p>where <span class="highlight mono">&lt;<i>board</i>&gt;</span> matches the board you want to target.
    The <span class="mono">-o bin/fft1d_offchip.aocx</span> argument is used to place the compiled binary
    in the location that the host program expects.
    </p>
<p>If you are unsure of the boards available, use the following command to list
available boards:</p>
<div class="command">aoc -list-boards</div>
<section>
<h3>Compiling for Emulator</h3>
<p>To use the emulation flow, the compilation command just needs to be modified slightly:</p>
<div class="command">aoc <span class="highlight nowrap">-march=emulator</span> device/fft1d_offchip.cl -o bin/fft1d_offchip.aocx<span class="nowrap"></span> <span class="nowrap">-fp-relaxed</span> -board=&lt;<i>board</i>&gt;</div>
</section>
<section>
<h3>Compiling for Fast Emulator</h3>
<p>To use the fast emulation flow, the compilation command just needs to be modified slightly:</p>
<div class="command">aoc <span class="highlight nowrap">-march=emulator -fast-emulator</span> device/fft1d_offchip.cl -o bin/fft1d_offchip.aocx<span class="nowrap"></span> <span class="nowrap">-fp-relaxed</span></div>
</section>
<section>
<h3>Compiling for Simulator</h3>
<p>To use the simulation flow, the compilation command just needs to be modified slightly:</p>
<div class="command">aoc <span class="highlight nowrap">-march=simulator</span> device/fft1d_offchip.cl -o bin/fft1d_offchip.aocx<span class="nowrap"></span> <span class="nowrap">-fp-relaxed</span> -board=&lt;<i>board</i>&gt;</div>
</section>
<section>
<h3>Kernel Preprocessor Definitions</h3>
<p>The kernel has the following preprocessor definitions:</p>
<table class="kernel-defines parameters">
<thead>
<tr>
  <th class="name">Define</th>
  <th class="type">Type</th>
  <th class="default">Default</th>
  <th class="desc">Description</th>
</tr>
</thead>
<tbody>
<tr>
  <td class="name">-D<span class="highlight">LOGN</span>=&lt;<i>#</i>&gt;</td>
  <td class="type">Optional</td>
  <td class="default">4</td>
  <td class="desc"><span class="mono">log(total number of points for cols) / 2</span>. The same value must be used
          for host and kernel compilation. LOGN must be greater then or equal to LOGM
        </td>
</tr>
<tr>
  <td class="name">-D<span class="highlight">LOGM</span>=&lt;<i>#</i>&gt;</td>
  <td class="type">Optional</td>
  <td class="default">LOGN</td>
  <td class="desc"><span class="mono">log(total number of points for rows) / 2</span>. The same value must be used
          for host and kernel compilation. 
        </td>
</tr>
<tr>
  <td class="name">-D<span class="highlight">LOGPOINTS</span>=&lt;<i>2,3,4</i>&gt;</td>
  <td class="type">Optional</td>
  <td class="default">2</td>
  <td class="desc"><span class="mono">log(number of points to process in parallel)</span>. The same value
          must be used for host and kernel compilation. LOGPOINTS must be strictly smaller than LOGN,LOGM.
        </td>
</tr>
</tbody>
</table>

</section>

<section>
<a id="Compiling_the_Host_Program"><h2>Compiling the Host Program</h2></a>
<p>To compile the host program, run:</p>
<div class="command">make</div>
<p>The compiled host program will be located at <span class="mono">bin/host</span>.</p>
<section>
<h3>Host Preprocessor Definitions</h3>
<p>The host program has the following preprocessor definitions:</p>
<table class="host-defines parameters">
<thead>
<tr>
  <th class="name">Define</th>
  <th class="type">Type</th>
  <th class="default">Default</th>
  <th class="desc">Description</th>
</tr>
</thead>
<tbody>
<tr>
  <td class="name">-D<span class="highlight">LOGN</span>=&lt;<i>#</i>&gt;</td>
  <td class="type">Optional</td>
  <td class="default">4</td>
  <td class="desc"><span class="mono">log(total number of points for cols) / 2</span>. The same value must be used
          for host and kernel compilation.
        </td>
</tr>
<tr>
  <td class="name">-D<span class="highlight">LOGM</span>=&lt;<i>#</i>&gt;</td>
  <td class="type">Optional</td>
  <td class="default">4</td>
  <td class="desc"><span class="mono">log(total number of points for rows) / 2</span>. The same value must be used
          for host and kernel compilation.
        </td>
</tr>
<tr>
  <td class="name">-D<span class="highlight">LOGPOINTS</span>=&lt;<i>2,3,4</i>&gt;</td>
  <td class="type">Optional</td>
  <td class="default">2</td>
  <td class="desc"><span class="mono">log(number of points to process in parallel)</span>. The same value
          must be used for host and kernel compilation.
        </td>
</tr>
</tbody>
</table>
<p>On Linux, custom values for preprocessor defines can be specified by setting 
the value of <mono>CPPFLAGS</mono> when invoking the Makefile.</p>

</section>

<section>
<a id="Running_the_Host_Program"><h2>Running the Host Program</h2></a>
<p>Before running the host program, you should have compiled the OpenCL kernel and the host program. Refer to the above sections if you have not completed those steps.</p>
<p>To run the host program on hardware, execute:</p>
<div class="command">bin/host</div>
<p>The output will include a wall-clock time of the OpenCL execution time
        and the kernel time as reported by the OpenCL event profiling API. The host
        program includes verification against the host CPU, printing out "PASS"
        when the results match.</p><section>
<h3>Running with the Emulator</h3>
<p>Prior to running the emulation flow, ensure that you have compiled the kernel for emulation. 
Refer to the above sections if you have not done so. Also, please set up your environment for
emulation. Please see the <a href="http://www.altera.com/literature/hb/opencl-sdk/aocl_programming_guide.pdf">Intel(R) FPGA SDK for OpenCL(TM) Programming Guide</a> for more information.</p>
<p>For this example design, the suggested emulation command is:</p>
<div class="command">CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 bin/host</div>
<section>
<h3>Running with the Fast Emulator</h3>
<p>Prior to running the fast emulation flow, ensure that you have compiled the kernel for fast emulation. 
Refer to the above sections if you have not done so. Also, please set up your environment for fast
emulation. Please see the <a href="http://www.altera.com/literature/hb/opencl-sdk/aocl_programming_guide.pdf">Intel(R) FPGA SDK for OpenCL(TM) Programming Guide</a> for more information.</p>
<p>For this example design, the suggested emulation command is:</p>
<div class="command">bin/host <span class="nowrap">-fast-emulator</span></div>
<section>
<h3>Running with the Simulator</h3>
<p>Prior to running the Simulation flow, ensure that you have compiled the kernel for simulation. 
Refer to the above sections if you have not done so. Also, please set up your environment for
simulation. Please see the <a href="http://www.altera.com/literature/hb/opencl-sdk/aocl_programming_guide.pdf">Intel(R) FPGA SDK for OpenCL(TM) Programming Guide</a> for more information.</p>
<p>For this example design, the suggested simulation command is:</p>
<div class="command">CL_CONTEXT_MPSIM_DEVICE_INTELFPGA=1 bin/host</div>
<section>
<h3>Host Parameters</h3>
<p>The general command-line for the host program is:</p>
<div class="command">bin/host <span class="nowrap">[-<span class="highlight">fast-emulator</span>]</span></div>
<p>where the one parameter is:</p>
<table class="host-params parameters">
<thead>
<tr>
  <th class="name">Parameter</th>
  <th class="type">Type</th>
  <th class="default">Default</th>
  <th class="desc">Description</th>
</tr>
</thead>
<tbody>
<tr>
  <td class="name">-<span class="highlight">fast-emulator</span></td>
  <td class="type">Optional</td>
  <td class="default"></td>
  <td class="desc">Select the fast emulator platform.</td>
</tr>
</tbody>
</table>
</section>
<section>
<h3>OpenCL Binary Selection</h3>
<p>The host program requires a OpenCL binary (AOCX) file to run. For this example design, OpenCL binary files should be placed in the 
<span class="mono">bin</span> directory.</p>

<p>By default, the host program will look for a binary file in the following order (earlier pattern matches 
take priority):</p>
<ol>
  <li>A file named <span class="mono">fft1d_offchip.aocx</span>.</li>
  <li>A file named <span class="mono">fft1d_offchip_<span class="highlight">&lt;<i>board</i>&gt;</span>_191.aocx</span>, 
  where <span class="highlight mono">&lt;<i>board</i>&gt;</span> is the name of the board (as passed as the 
  <span class="mono">-board</span> argument to <span class="mono">aoc</span>).</li>
</ol>
</section>

</section>

<section>
<a id="Release_History"><h2>Release History</h2></a>
<p/>
<table class="history">
<thead>
<tr>
  <th class="version">Example Version</th>
  <th class="sdk-version">SDK Version</th>
  <th class="date">Date</th>
  <th class="changes">Changes</th>
</tr>
</thead>
<tbody>
<tr>
  <td class="version">1.5</td>
  <td class="sdk-version">18.1</td>
  <td class="date">October 2018</td>
  <td class="changes"><ul><li>Add simulator option.</li></ul></td>
</tr>
<tr>
  <td class="version">1.4</td>
  <td class="sdk-version">18.1</td>
  <td class="date">August 2018</td>
  <td class="changes"><ul><li>Add fast emulator option.</li></ul></td>
</tr>
<tr>
  <td class="version">1.3</td>
  <td class="sdk-version">18.0</td>
  <td class="date">May 2018</td>
  <td class="changes"><ul><li>Change: Design has been optimized for Stratix 10.</li></ul></td>
</tr>
<tr>
  <td class="version">1.2</td>
  <td class="sdk-version">16.2</td>
  <td class="date">December 2016</td>
  <td class="changes"><ul><li>Big change: added multi-wire transpose (MWT) kernels to do in-place transpositions of data for
        both fetch and transpose kernels. The MWT kernels do not need any on-chip memory replication as was
        required by the previous version while still allowing POINTS number of points to be read and written
        each clock cycle. This saves precious on-chip RAM.</li></ul></td>
</tr>
<tr>
  <td class="version">1.1</td>
  <td class="sdk-version">16.0</td>
  <td class="date">June 2016</td>
  <td class="changes"><ul><li>Fixed makefile.</li></ul></td>
</tr>
<tr>
  <td class="version">1.0</td>
  <td class="sdk-version">14.1</td>
  <td class="date">December 2014</td>
  <td class="changes"><ul><li>First release.</li></ul></td>
</tr>
</tbody>
</table>

</section>

<section>
<a id="Legal"><h2>Legal</h2></a>
<pre class="license">Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

This agreement shall be governed in all respects by the laws of the State of California and
by the laws of the United States of America.
</pre><section><h3>Trademarks</h3><div class="trademark"><p>OpenCL and the OpenCL logo are trademarks of Apple Inc. used by permission by Khronos.</p><p>Product is based on a published Khronos Specification, and has passed the Khronos Conformance Testing Process. Current conformance status can be found at <a href="www.khronos.org/conformance">www.khronos.org/conformance</a>.</p></div></section>
</section>

<section>
<a id="Contacting_Intel"><h2>Contacting Intel</h2></a>
<p>Although we have made every effort to ensure that this design example works
correctly, there might be problems that we have not encountered. If you have
a question or problem that is not answered by the information provided in 
this readme file or the example's documentation, please contact Intel
support (<a href="http://www.altera.com/myaltera">myAltera</a>).</p>

</section>

</body>
</html>
