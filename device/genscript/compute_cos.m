function cos_buf = compute_cos(fid, depth, increment, offset, modvalue, N)
for ii=1:depth
    index = mod(((ii-1)*increment+offset)*2*pi/N, modvalue);
    if (ii<depth)
        fprintf(fid,'%2.11ff, ', cos(index));
    else
        fprintf(fid,'%2.11ff};\n', cos(index));
    end
    
    cos_buf(ii) = cos(index);
end
