function generate_twid_mod_8p(LOGN)
P = 8;          %% Parallel Phases
N=2^LOGN;       %% Precompute for FFT sizes between 8 and 4096 points
DEPTH = N/P;
num_stages = LOGN/2 - 1;

assert(0==mod(LOGN,2),'LOGN must be even integer');
assert(N>0 && N>=P,'LOGN too small');

fid=fopen('twid_8.h','w');
fprintf(fid,'// This is a script generated file\n');
fprintf(fid,'// You probably want to edit generate_twid_8p.m\n\n');

fprintf(fid,'#define TWID_STAGES %d\n\n', num_stages);

%%%%%%%%%%%%%%%%%%%%%%
%% Generate cosine
%%%%%%%%%%%%%%%%%%%%%%

for no_stage=1:num_stages
    
    base_index = 4^(no_stage-1);
    
    fprintf(fid,'constant float tc%d0[%d] = {',no_stage-1, DEPTH);
    tc(1, :, no_stage) = compute_cos(fid, DEPTH, 2*base_index, 0, pi, N);

    fprintf(fid,'constant float tc%d3[%d] = {',no_stage-1, DEPTH);
    tc(2, :, no_stage) = compute_cos(fid, DEPTH, 2*base_index, (2*DEPTH)*4^(no_stage-1), pi, N);
    
    fprintf(fid,'constant float tc%d1[%d] = {',no_stage-1, DEPTH);
    tc(3, :, no_stage) = compute_cos(fid, DEPTH, base_index, 0, pi/2, N);

    fprintf(fid,'constant float tc%d4[%d] = {',no_stage-1, DEPTH);
    tc(4, :, no_stage) = compute_cos(fid, DEPTH, base_index, (1*DEPTH)*4^(no_stage-1), pi/2, N);

    fprintf(fid,'constant float tc%d2[%d] = {',no_stage-1, DEPTH);
    tc(5, :, no_stage) = compute_cos(fid, DEPTH, 3*base_index, 0, 3*pi/2, N);

    fprintf(fid,'constant float tc%d5[%d] = {',no_stage-1, DEPTH);
    tc(6, :, no_stage) = compute_cos(fid, DEPTH, 3*base_index, (3*DEPTH)*4^(no_stage-1), 3*pi/2, N);
end


%%%%%%%%%%%%%%%%%%%%%%
%% Generate sine
%%%%%%%%%%%%%%%%%%%%%%
fprintf(fid,'\n\n\n');

for no_stage=1:num_stages
    
    base_index = 4^(no_stage-1);
    
    fprintf(fid,'constant float ts%d0[%d] = {',no_stage-1, DEPTH);
    ts(1, :, no_stage) = compute_sin(fid, DEPTH, 2*base_index, 0, pi, N);

    fprintf(fid,'constant float ts%d3[%d] = {',no_stage-1, DEPTH);
    ts(2, :, no_stage) = compute_sin(fid, DEPTH, 2*base_index, (2*DEPTH)*4^(no_stage-1), pi, N);

    fprintf(fid,'constant float ts%d1[%d] = {',no_stage-1, DEPTH);
    ts(3, :, no_stage) = compute_sin(fid, DEPTH, base_index, 0, pi/2, N);

    fprintf(fid,'constant float ts%d4[%d] = {',no_stage-1, DEPTH);
    ts(4, :, no_stage) = compute_sin(fid, DEPTH, base_index, (1*DEPTH)*4^(no_stage-1), pi/2, N);

    fprintf(fid,'constant float ts%d2[%d] = {',no_stage-1, DEPTH);
    ts(5, :, no_stage) = compute_sin(fid, DEPTH, 3*base_index, 0, 3*pi/2, N);

    fprintf(fid,'constant float ts%d5[%d] = {',no_stage-1, DEPTH);
    ts(6, :, no_stage) = compute_sin(fid, DEPTH, 3*base_index, (3*DEPTH)*4^(no_stage-1), 3*pi/2, N);
end
