function generate_twid_mod_16p(LOGN)

P = 16;         %% Parallel Phases
N = 2^LOGN;       %% Precompute for FFT sizes between P and N points
DEPTH = N/P;
num_stages = LOGN/2 - 1;

assert(0==mod(LOGN,2),'LOGN must be even integer');
assert(N>0 && N>=P,'LOGN too small');

fid=fopen('twid_16.h','w');
fprintf(fid,'// This is a script generated file\n');
fprintf(fid,'// You probably want to edit generate_twid_mod_16p.m\n\n');

fprintf(fid,'#define TWID_STAGES %d\n\n', num_stages);

%%%%%%%%%%%%%%%%%%%%%%
%% Generate cosine
%%%%%%%%%%%%%%%%%%%%%%

for no_stage=1:num_stages
    
    base_index = 4^(no_stage-1);
    
    fprintf(fid,'constant float tc%d00[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 2*base_index, 0, pi, N);

    fprintf(fid,'constant float tc%d03[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 2*base_index, (DEPTH*4)*4^(no_stage-1), pi, N);
    
    fprintf(fid,'constant float tc%d06[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 2*base_index, (DEPTH*2)*4^(no_stage-1), pi, N);
    
    fprintf(fid,'constant float tc%d09[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 2*base_index, (DEPTH*6)*4^(no_stage-1), pi, N);

    fprintf(fid,'constant float tc%d01[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, base_index, 0, pi/2, N);

    fprintf(fid,'constant float tc%d04[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, base_index, (DEPTH*2)*4^(no_stage-1), pi/2, N);
    
    fprintf(fid,'constant float tc%d07[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, base_index, (DEPTH*1)*4^(no_stage-1), pi/2, N);
    
    fprintf(fid,'constant float tc%d10[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, base_index, (DEPTH*3)*4^(no_stage-1), pi/2, N);

    fprintf(fid,'constant float tc%d02[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 3*base_index, 0, 3*pi/2, N);

    fprintf(fid,'constant float tc%d05[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 3*base_index, (DEPTH*6)*4^(no_stage-1), 3*pi/2, N);
    
    fprintf(fid,'constant float tc%d08[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 3*base_index, (DEPTH*3)*4^(no_stage-1), 3*pi/2, N);
    
    fprintf(fid,'constant float tc%d11[%d] = {',no_stage-1, DEPTH);
    compute_cos(fid, DEPTH, 3*base_index, (DEPTH*9)*4^(no_stage-1), 3*pi/2, N);
end


%%%%%%%%%%%%%%%%%%%%%%
%% Generate sine
%%%%%%%%%%%%%%%%%%%%%%
fprintf(fid,'\n\n\n');

for no_stage=1:num_stages
    
    base_index = 4^(no_stage-1);
    
    fprintf(fid,'constant float ts%d00[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 2*base_index, 0, pi, N);

    fprintf(fid,'constant float ts%d03[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 2*base_index, (DEPTH*4)*4^(no_stage-1), pi, N);
    
    fprintf(fid,'constant float ts%d06[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 2*base_index, (DEPTH*2)*4^(no_stage-1), pi, N);
    
    fprintf(fid,'constant float ts%d09[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 2*base_index, (DEPTH*6)*4^(no_stage-1), pi, N);

    fprintf(fid,'constant float ts%d01[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, base_index, 0, pi/2, N);

    fprintf(fid,'constant float ts%d04[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, base_index, (DEPTH*2)*4^(no_stage-1), pi/2, N);
    
    fprintf(fid,'constant float ts%d07[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, base_index, (DEPTH*1)*4^(no_stage-1), pi/2, N);
    
    fprintf(fid,'constant float ts%d10[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, base_index, (DEPTH*3)*4^(no_stage-1), pi/2, N);

    fprintf(fid,'constant float ts%d02[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 3*base_index, 0, 3*pi/2, N);

    fprintf(fid,'constant float ts%d05[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 3*base_index, (DEPTH*6)*4^(no_stage-1), 3*pi/2, N);
    
    fprintf(fid,'constant float ts%d08[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 3*base_index, (DEPTH*3)*4^(no_stage-1), 3*pi/2, N);
    
    fprintf(fid,'constant float ts%d11[%d] = {',no_stage-1, DEPTH);
    compute_sin(fid, DEPTH, 3*base_index, (DEPTH*9)*4^(no_stage-1), 3*pi/2, N);
end
