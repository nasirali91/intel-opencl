function sin_buf = compute_sin(fid, depth, increment, offset, modvalue, N)
for ii=1:depth
    index = mod(((ii-1)* increment + offset)*2*pi/N, modvalue);
    if (ii<depth)
        fprintf(fid,'%2.12ff, ', -sin(index));
    else
        fprintf(fid,'%2.12ff};\n', -sin(index));
    end
    sin_buf(ii) = -sin(index);
end
