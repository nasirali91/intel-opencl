// Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

// Complex single-precision floating-point radix-4 feedforward FFT / iFFT engine
// 
// See Mario Garrido, Jesús Grajal, M. A. Sanchez, Oscar Gustafsson:
// Pipeline Radix-2k Feedforward FFT Architectures. 
// IEEE Trans. VLSI Syst. 21(1): 23-32 (2013))
//
// The log(size) of the transform must be a compile-time constant argument. 
// This FFT engine processes 8 points for each invocation. The inputs are eight 
// ordered streams while the outputs are in bit reversed order.
//
// The entry point of the engine is the 'fft_step' function. This function
// passes 8 data points through a fixed sequence of processing blocks
// (butterfly, rotation, swap, reorder, multiplications, etc.) and produces
// 8 output points towards the overall FFT transform. 
//
// The engine is designed to be invoked from a loop in a single work-item task. 
// When compiling a single work-item task, the compiler leverages pipeline 
// parallelism and overlaps the execution of multiple invocations of this 
// function. A new instance can start processing every clock cycle



// Includes tabled twiddle factors - storing constants uses fewer resources
// than instantiating 'cos' or 'sin' hardware
#include "twid_8.h" 

// FFT butterfly building block
float2x8 butterfly(float2x8 data) {
   float2x8 res;
   res.i0 = data.i0 + data.i1;
   res.i1 = data.i0 - data.i1;
   res.i2 = data.i2 + data.i3;
   res.i3 = data.i2 - data.i3;
   res.i4 = data.i4 + data.i5;
   res.i5 = data.i4 - data.i5;
   res.i6 = data.i6 + data.i7;
   res.i7 = data.i6 - data.i7;
   return res;
}

// Swap real and imaginary components in preparation for inverse transform
float2x8 swap_complex(float2x8 data) {
   float2x8 res;
   res.i0.x = data.i0.y;
   res.i0.y = data.i0.x;
   res.i1.x = data.i1.y;
   res.i1.y = data.i1.x;
   res.i2.x = data.i2.y;
   res.i2.y = data.i2.x;
   res.i3.x = data.i3.y;
   res.i3.y = data.i3.x;
   res.i4.x = data.i4.y;
   res.i4.y = data.i4.x;
   res.i5.x = data.i5.y;
   res.i5.y = data.i5.x;
   res.i6.x = data.i6.y;
   res.i6.y = data.i6.x;
   res.i7.x = data.i7.y;
   res.i7.y = data.i7.x;
   return res;
}

// FFT trivial rotation building block
float2x8 trivial_rotate(float2x8 data) {
   TYPE tmp = data.i3;
   data.i3.x = tmp.y;
   data.i3.y = -tmp.x;
   tmp = data.i7;
   data.i7.x = tmp.y;
   data.i7.y = -tmp.x;
   return data;
}

// FFT data swap building block associated with trivial rotations
float2x8 trivial_swap(float2x8 data) {
   TYPE tmp = data.i1;
   data.i1 = data.i2;
   data.i2 = tmp;
   tmp = data.i5;
   data.i5 = data.i6;
   data.i6 = tmp;
   return data;
}

// FFT data swap building block associated with complex rotations
float2x8 swap(float2x8 data) {
   TYPE tmp = data.i1;
   data.i1 = data.i4;
   TYPE tmp2 = data.i2;
   data.i2 = tmp;
   tmp = data.i3;
   data.i3 = data.i5;
   data.i4 = tmp2;
   data.i5 = data.i6;
   data.i6 = tmp;
   return data;
}

// This function "delays" the input by 'depth' steps
// Input 'data' from invocation N would be returned in invocation N + depth
// The 'shift_reg' sliding window is shifted by 1 element at every invocation 
float2x4 delay_data(float2x4 data, const uint depth, const uint depth_mod_mask, 
                local float2x4 *shift_reg, uint inv_count) {
   uint read_addr  = (0 + inv_count)     & depth_mod_mask; 
   uint write_addr = (depth + inv_count) & depth_mod_mask; 
   shift_reg[write_addr] = data;
   return shift_reg[read_addr];
}

// FFT data reordering building block. Implements the reordering depicted below 
// (for depth = 2). The first valid outputs are in invocation 4
// Invocation count: 0123...          01234567...
// data.i0         : GECA...   ---->      DBCA...
// data.i1         : HFDB...   ---->      HFGE...
float2x8 reorder_data4(float2x8 data, const uint depth, const uint depth_mod_mask,
      local TYPE *delay1, 
      local TYPE *delay2, 
      uint inv_count, const uint stage, bool toggle) {
   // Use disconnected segments of length 'depth + 1' elements starting at 
   // 'shift_reg' to implement the delay elements. At the end of each FFT step, 
   // the contents of the entire buffer is shifted by 1 element
   float2x4 t;
   t.i0 = data.i1;
   t.i1 = data.i3;
   t.i2 = data.i5;
   t.i3 = data.i7;
   t = delay_data(t, depth, depth_mod_mask, (local float2x4*)delay1, inv_count);
   data.i1 = t.i0;
   data.i3 = t.i1;
   data.i5 = t.i2;
   data.i7 = t.i3;
   
   if (toggle) {
      TYPE tmp = data.i0;
      data.i0 = data.i1;
      data.i1 = tmp;
      tmp = data.i2;
      data.i2 = data.i3;
      data.i3 = tmp;
      tmp = data.i4;
      data.i4 = data.i5;
      data.i5 = tmp;
      tmp = data.i6;
      data.i6 = data.i7;
      data.i7 = tmp;
   }

   t.i0 = data.i0;
   t.i1 = data.i2;
   t.i2 = data.i4;
   t.i3 = data.i6;
   t = delay_data(t, depth, depth_mod_mask, (local float2x4*)delay2, inv_count);
   data.i0 = t.i0;
   data.i2 = t.i1;
   data.i4 = t.i2;
   data.i6 = t.i3;
   
   return data;
}

// Produces the twiddle factor associated with a processing stream 'stream', 
// at a specified 'stage' during a step 'index' of the computation
//
// If there are precomputed twiddle factors for the given FFT size, uses them
// This saves hardware resources, because it avoids evaluating 'cos' and 'sin'
// functions

TYPE twiddle(uint index, uint stage, uint log_size, uint stream) {
   TYPE twid;
   // Coalesces the twiddle tables for indexed access
   constant float * twiddles_cos[TWID_STAGES][6] = {
                        {tc00, tc01, tc02, tc03, tc04, tc05}, 
                        {tc10, tc11, tc12, tc13, tc14, tc15}, 
                        {tc20, tc21, tc22, tc23, tc24, tc25}, 
                        {tc30, tc31, tc32, tc33, tc34, tc35}, 
                        {tc40, tc41, tc42, tc43, tc44, tc45},
                        {tc50, tc51, tc52, tc53, tc54, tc55},
                        {tc60, tc61, tc62, tc63, tc64, tc65}
   };
   constant float * twiddles_sin[TWID_STAGES][6] = {
                        {ts00, ts01, ts02, ts03, ts04, ts05}, 
                        {ts10, ts11, ts12, ts13, ts14, ts15}, 
                        {ts20, ts21, ts22, ts23, ts24, ts25}, 
                        {ts30, ts31, ts32, ts33, ts34, ts35}, 
                        {ts40, ts41, ts42, ts43, ts44, ts45},
                        {ts50, ts51, ts52, ts53, ts54, ts55},
                        {ts60, ts61, ts62, ts63, ts64, ts65}
   };

   // Use the precomputed twiddle fators, if available
   uint twid_stage = stage >> 1;
   if (log_size <= (TWID_STAGES * 2 + 2)) {
      uint index_mult = 1 << (TWID_STAGES * 2 + 2 - log_size);
      twid.x = twiddles_cos[twid_stage][stream][index * index_mult];
      twid.y = twiddles_sin[twid_stage][stream][index * index_mult];
   }
   return twid;
}


float2x6 get_twid(uint index, uint stage, uint log_size) {
  float2x6 result;
  result.i0 = twiddle(index, stage, log_size, 0);
  result.i1 = twiddle(index, stage, log_size, 1);
  result.i2 = twiddle(index, stage, log_size, 2);
  result.i3 = twiddle(index, stage, log_size, 3);
  result.i4 = twiddle(index, stage, log_size, 4);
  result.i5 = twiddle(index, stage, log_size, 5);
  return result;
}

float2x8 complex_rotate_given_twid(float2x8 data, float2x6 twid) {
   data.i1 = comp_mult(data.i1, twid.i0);
   data.i2 = comp_mult(data.i2, twid.i1);
   data.i3 = comp_mult(data.i3, twid.i2);
   data.i5 = comp_mult(data.i5, twid.i3);
   data.i6 = comp_mult(data.i6, twid.i4);
   data.i7 = comp_mult(data.i7, twid.i5);
   return data;
}

// FFT complex rotation building block
float2x8 complex_rotate(float2x8 data, uint index, uint stage, uint log_size, bool bypass) {
  float2x6 twid = get_twid(index, stage, log_size);
  if (bypass) {
    twid.i0.x = 1.0f; twid.i0.y = 0.0f;
    twid.i1.x = 1.0f; twid.i1.y = 0.0f;
    twid.i2.x = 1.0f; twid.i2.y = 0.0f;
    twid.i3.x = 1.0f; twid.i3.y = 0.0f;
    twid.i4.x = 1.0f; twid.i4.y = 0.0f;
    twid.i5.x = 1.0f; twid.i5.y = 0.0f;
  }
  return complex_rotate_given_twid(data, twid);
}

typedef struct {
  float2x8 data;
  uint size;
  uint logM;
  uint step;
  uint inv_count;
  uint two_to_logM_m_stage;
  uint delay;
  uint delay_mod_mask;
} interstage_data;

interstage_data do_single_stage (const uint stage, interstage_data d, 
                                 local TYPE *delay1,
                                 local TYPE *delay2)
{
  bool complex_stage = stage & 1;

  // Figure out the index of the element processed at this stage
  // Subtract (add modulo size / 8) the delay incurred as data travels 
  // from one stage to the next
  bool process_stage = (stage < (d.logM - 1));
  
  uint data_index = (d.step + d.two_to_logM_m_stage) & ((d.size >> 3) - 1);      
          
  if (process_stage) d.data = butterfly(d.data);

  if (complex_stage) {
    d.data = complex_rotate(d.data, data_index, stage, d.logM, !process_stage);
  }

  if (process_stage) d.data = swap(d.data);

  // Reordering multiplexers must toggle every 'delay' steps
  bool toggle = data_index & d.delay;

  // Assign unique sections of the buffer for the set of delay elements at
  // each stage
  uint delay_arg = process_stage ? d.delay : 0;
  uint toggle_arg = process_stage ? toggle : 0;
  d.data = reorder_data4(d.data, delay_arg, d.delay_mod_mask, delay1, delay2, d.inv_count, stage, toggle_arg);

  if (!complex_stage && process_stage) {
    d.data = trivial_rotate(d.data);
  }
  
  // update for next call
  d.two_to_logM_m_stage >>= 1;
  d.delay >>= 1;
  d.delay_mod_mask >>= 1;
  
  return d;
}


// Process 8 input points towards and a FFT/iFFT of size N, N >= 8 
// (in order input, bit reversed output). Apply all input points in N / 8 
// consecutive invocations. Obtain all outputs in N /8 consecutive invocations 
// starting with invocation N /8 - 1 (outputs are delayed). Multiple back-to-back 
// transforms can be executed
//
// 'data' encapsulates 8 complex single-precision floating-point input points
// 'step' specifies the index of the current invocation 
// 'delay' is an array representing a sliding window of size N+8*(log(N)-2)
// 'inverse' toggles between the direct and inverse transform
// 'logN' should be a COMPILE TIME constant evaluating log(N) - the constant is 
//        propagated throughout the code to achieve efficient hardware
//
float2x8 fft_step(float2x8 data, uint inv_count, uint step, 
                  local TYPE *delay1, local TYPE *delay11,
                  local TYPE *delay2, local TYPE *delay21,
                  local TYPE *delay3, local TYPE *delay31,
                  local TYPE *delay4, local TYPE *delay41,
                  local TYPE *delay5, local TYPE *delay51,
                  local TYPE *delay6, local TYPE *delay61,
                  local TYPE *delay7, local TYPE *delay71,
                  local TYPE *delay8, local TYPE *delay81,
                  local TYPE *delay9, local TYPE *delay91,
                  local TYPE *delayA, local TYPE *delayA1,
                  local TYPE *delayB, local TYPE *delayB1,
                  local TYPE *delayC, local TYPE *delayC1,
                  local TYPE *delayD, local TYPE *delayD1, // unused. Here to have the same signature as fft_step in fft_4.cl
                  bool inverse, const uint logN, const uint logM, const uint size) {

    // Swap real and imaginary components if doing an inverse transform
    if (inverse) {
       data = swap_complex(data);
    }

    // Stage 0 of feed-forward FFT
    data = butterfly(data);
    data = trivial_rotate(data);
    data = trivial_swap(data);
    
    // Stage 1
    data = butterfly(data);
    data = complex_rotate(data, step & ((size >> 3) - 1), 1, logM, false /* do not bypass */);
    data = swap(data);

    // Next logN - 2 stages alternate two computation patterns
    interstage_data d;
    d.data = data;
    d.size = size;
    d.logM = logM;
    d.step = step;
    d.inv_count = inv_count;
    d.two_to_logM_m_stage = size >> 3; // power_of_2 (logM - 1 - stage);
    d.delay = size >> 4; // power_of_2 (logM - 2 - stage);
    d.delay_mod_mask = (size >> 3) - 1; // 2*delay - 1;
    
#if LOGN>3
    d = do_single_stage (2, d, delay1, delay11);
#endif
#if LOGN>4
    d = do_single_stage (3, d, delay2, delay21);
#endif
#if LOGN>5
    d = do_single_stage (4, d, delay3, delay31);
#endif
#if LOGN>6
    d = do_single_stage (5, d, delay4, delay41);
#endif
#if LOGN>7
    d = do_single_stage (6, d, delay5, delay51);
#endif
#if LOGN>8
    d = do_single_stage (7, d, delay6, delay61);
#endif
#if LOGN>9
    d = do_single_stage (8, d, delay7, delay71);
#endif
#if LOGN>10
    d = do_single_stage (9, d, delay8, delay81);
#endif
#if LOGN>11
    d = do_single_stage (10, d, delay9, delay91);
#endif
#if LOGN>12
    d = do_single_stage (11, d, delayA, delayA1);
#endif
#if LOGN>13
    d = do_single_stage (12, d, delayB, delayB1);
#endif
#if LOGN>14
    d = do_single_stage (13, d, delayC, delayB1);
#endif
#if LOGN>15
    #error "LOGN is too big.\nYou need to regenerate constant twiddle factors to support larger sizes."
#endif

    data = d.data;

    // Stage logN - 1
    data = butterfly(data);

    if (inverse) {
       data = swap_complex(data);
    }

    return data;
}

