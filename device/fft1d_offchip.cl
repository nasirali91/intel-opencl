// Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

/* This is the top-level device source file for 1D FFT using external memory, 
 * optimized for Stratix 10.
 * 
 * See accompanying README for high-level implementation information.
 * The design is essentially a 2D FFT with an extra transpose and twiddle 
 * multiplication.
 *
 * This module performs a specified number of on-chip FFTs of specified size
 * and is parameterized for point parallelism.
 *
 * A single FFT engine can process rows and columns back-to-back. However, as 
 * matrix data is stored in global memory, the efficiency of memory accesses 
 * will impact the overall performance. Accessing consecutive memory 
 * locations leads to efficient access patterns. However, this is obviously 
 * not possible when accessing both rows and columns.
 * 
 * The implementation is divided between three concurrent OpenCL kernels, as 
 * depicted below:
 *
 *  ---------------------      ---------------      -------------------------
 *  | read matrix rows, | ---> |  FFT engine | ---> | bit-reverse, transpose |
 *  |  form 8 streams   |  x8  |    (task)   |  x8  |    and write matrix    |
 *  |    (ND range)     | ---> |             | ---> |      (ND range)        |
 *  ---------------------      ---------------      --------------------------
 *
 * This sequence of kernels does back-to-back row processing followed by a 
 * data transposition and writes the results back to memory. The host code 
 * runs these kernels twice to produce the overall 2D FFT transform
 *
 * The FFT engine is implemented as an OpenCL single work-item task (see 
 * fft1d example for details) while the data reordering kernels, reading and 
 * writing the matrix data from / to memory, are implemented as ND range 
 * kernels. 
 *
 * These kernels transfer data through channels, an Intel FPGA Vendor extension 
 * that allows for direct communication between kernels or between kernels. 
 * This avoids the need to read and write intermediate data using global 
 * memory as in traditional OpenCL implementations.
 *
 * In many cases the FFT engine is a building block in a large application. In
 * this case, the memory layout of the matrix can be altered to achieve higher
 * memory transfer efficiency. This implementation demonstrates how an 
 * alternative memory layout can improve performance. The host switches 
 * between the two memory layouts using a kernel argument. See the 
 * 'mangle_bits' function for additional details.
 */

// Include source code for an engine that produces 8 points each step


// Main static parameters of FFT are in this file
#include "../host/inc/fft_config.h"

// Declare channels for kernel to kernel communication

#pragma OPENCL EXTENSION cl_intel_channels : enable

#include "multiwire_transpose.cl"
#include "channels.h"
#include "util.h"

// Smallest value of LOGM that the generated core will support.
// If equal to LOGM, the resulting core will support only LOGM sizes.
// If set to less than LOGM, will dynamically support sizes from 
// MIN_LOGM to LOGM.

// You should predefine MIN_LOGNM if you want the same built FFT code to cope
// with both 'large' and 'small' FFT sizes - it may then force use of sincos
// even when the larger FFTs would not need them. See usage below.
#ifndef MIN_LOGNM
#define MIN_LOGNM LOGM
#endif

#define FFT_DELAY_ARR_SIZE (1<<(LOGN))

#if (POINTS == 16)
  #include "fft_16.h"
#elif  (POINTS == 8)
  #include "fft_8.h"
#elif (POINTS == 4)
  #include "fft_4.h"
#endif


/* This kernel reads the matrix data and provides POINTS parallel streams to
 * the FFT engine. 
 */
__attribute__((max_global_work_dim(0)))
kernel void fetch(global volatile float2  * restrict src, int mangle, int transpose, 
                  uint  log_column_count, uint log_row_count,
                  uint column_count, uint row_count) {

// Allow the size to be fixed - optimization at the expense of flexibility.
#ifdef FIXED_N
  log_column_count = LOGN;
  column_count = 1 << LOGN;
#endif
#ifdef FIXED_M
  log_row_count = LOGM;
  row_count = 1 << LOGM;
#endif

  for (int i=0; i < (column_count * row_count/POINTS); i += column_count) {
    float_vector data __attribute__((register));
    for (int j=0; j < column_count * SPLIT_MEM; j++) {
      
      int id = i + (j / SPLIT_MEM);
      int rows, columns;
      int where_global;

      where_global = id << LOGPOINTS;

      if (transpose) {
         // where_global = [row][col][ch]  -->  [col][row][ch]
        columns = var_rsh (where_global, log_column_count) & (~((1 << LOGPOINTS) - 1));
        rows = id & (column_count-1);
        where_global = rows * row_count + columns;
      }

      if (mangle) {
        where_global = mangle_bits(where_global, log_column_count + log_row_count);
      }

      // Read POINTS points in a single coalesced access - the cast to ulong is 
      // performed to avoid a rare coalescing issue preventing the compiler from 
      // fully coalescing the accesses - this will be fixed in a later version of 
      // the compiler.
      // 

      where_global = (where_global / POINTS) * POINTS + (j % SPLIT_MEM) * (POINTS / SPLIT_MEM);
      #pragma unroll
      for (int ch = 0; ch < (POINTS / SPLIT_MEM); ch++) { 
        if (j % SPLIT_MEM == 0) data.d[ch] = src[where_global + ch];
        if (j % SPLIT_MEM == 1) {
           data.d[POINTS / SPLIT_MEM + ch] = src[where_global + ch];
        }
      }
      if (j % SPLIT_MEM == SPLIT_MEM - 1) {
         write_channel_intel(chan_mwt_in[0], data);
      }
    } // end for loop j
  } // end for loop i
}


/* This single work-item task wraps the FFT engine
 * 'inverse' toggles between the direct and the inverse transform
 */
__attribute__((max_global_work_dim(0)))
kernel void fft1d(uint inverse, uint log_column_count, uint log_row_count,
                                uint column_count_arg, uint row_count) {

// Allow the size to be fixed - optimization at the expense of flexibility.
#ifdef FIXED_N
  column_count_arg = 1 << LOGN;
  log_column_count = LOGN;
#endif
#ifdef FIXED_M
  row_count = 1 << LOGN;
  log_row_count = LOGM;
#endif

  uint column_count = column_count_arg >> LOGPOINTS;


  /* The FFT engine requires a sliding window for data reordering; 
   * These arrays implement a fifo (two arrays for each stage) that
   * effectively gives the desired reordering.
   * If you need to support larger LOGN, add more such delay[] arrays. */
  local float2 delay1 [FFT_DELAY_ARR_SIZE];
  local float2 delay11[FFT_DELAY_ARR_SIZE];
  local float2 delay2 [FFT_DELAY_ARR_SIZE >> 1];
  local float2 delay21[FFT_DELAY_ARR_SIZE >> 1];
  local float2 delay3 [FFT_DELAY_ARR_SIZE >> 2];
  local float2 delay31[FFT_DELAY_ARR_SIZE >> 2];
  local float2 delay4 [FFT_DELAY_ARR_SIZE >> 3];
  local float2 delay41[FFT_DELAY_ARR_SIZE >> 3];
  local float2 delay5 [FFT_DELAY_ARR_SIZE >> 4];
  local float2 delay51[FFT_DELAY_ARR_SIZE >> 4];
  local float2 delay6 [FFT_DELAY_ARR_SIZE >> 5];
  local float2 delay61[FFT_DELAY_ARR_SIZE >> 5];
  local float2 delay7 [FFT_DELAY_ARR_SIZE >> 6];
  local float2 delay71[FFT_DELAY_ARR_SIZE >> 6];
  local float2 delay8 [FFT_DELAY_ARR_SIZE >> 7];
  local float2 delay81[FFT_DELAY_ARR_SIZE >> 7];
  local float2 delay9 [FFT_DELAY_ARR_SIZE >> 8];
  local float2 delay91[FFT_DELAY_ARR_SIZE >> 8];
  local float2 delayA [FFT_DELAY_ARR_SIZE >> 9];
  local float2 delayA1[FFT_DELAY_ARR_SIZE >> 9];
  local float2 delayB [FFT_DELAY_ARR_SIZE >> 10];
  local float2 delayB1[FFT_DELAY_ARR_SIZE >> 10];
  local float2 delayC [FFT_DELAY_ARR_SIZE >> 11];
  local float2 delayC1[FFT_DELAY_ARR_SIZE >> 11];
  local float2 delayD [FFT_DELAY_ARR_SIZE >> 12];
  local float2 delayD1[FFT_DELAY_ARR_SIZE >> 12];

  // needs to run "N / 8 - 1" additional iterations to drain the last outputs
  for (unsigned i = 0; i < (row_count + 1) * column_count - 1; i++) {
    float_vector data;

    // Read data from channels
   if (i < row_count * column_count) {
      data = read_channel_intel(chan_mwt_out[0]); 

    } else {
      #pragma unroll
      for(int i = 0; i < POINTS; ++i)
          data.d[i] = 0;
    }

    // Perform one FFT step   
    data = fft_step(data, i, (i & (column_count-1)), 
      (local float2 *)&delay1,      (local float2 *)&delay11,
      (local float2 *)&delay2,      (local float2 *)&delay21,
      (local float2 *)&delay3,      (local float2 *)&delay31,
      (local float2 *)&delay4,      (local float2 *)&delay41,
      (local float2 *)&delay5,      (local float2 *)&delay51,
      (local float2 *)&delay6,      (local float2 *)&delay61,
      (local float2 *)&delay7,      (local float2 *)&delay71,
      (local float2 *)&delay8,      (local float2 *)&delay81,
      (local float2 *)&delay9,      (local float2 *)&delay91,
      (local float2 *)&delayA,      (local float2 *)&delayA1,
      (local float2 *)&delayB,      (local float2 *)&delayB1,
      (local float2 *)&delayC,      (local float2 *)&delayC1,
      (local float2 *)&delayD,      (local float2 *)&delayD1,
      inverse, LOGN, log_column_count, column_count << LOGPOINTS);

    // Write result to channels
    if (i >= column_count - 1) {
      write_channel_intel(chan_mwt_in[1], data);
    }
  }
}


/* Accept points from channel and write them to memory, in the required access
 * pattern.
 * Apply any further twiddle necessary to support large 1D FFTs (i.e. at the
 * end of the first pass of the required two).
 */

__attribute__((max_global_work_dim(0)))
kernel void transpose(global volatile float2 * restrict dest, int mangle, int twiddle, int inverse,
                      uint log_column_count, uint log_row_count,
                      uint column_count, uint row_count,
                      float delta_const) {

// Allow the size to be fixed - optimization at the expense of flexibility.
#ifdef FIXED_N
  log_column_count = LOGN;
  column_count = 1 << LOGN;
#endif
#ifdef FIXED_M
  log_row_count = LOGM;
  row_count = 1 << LOGM;
#endif

#pragma ivdep
  for (int index_i=0; index_i < (column_count * row_count/POINTS); index_i += column_count) {
    float_vector __attribute__((aligned(sizeof(float_vector)))) __attribute__((register)) data;  
#pragma ivdep
    for (int index_j=0; index_j < column_count * SPLIT_MEM; index_j++) {
     
      // Each write handles POINTS points
      int colt = index_j / SPLIT_MEM;
      int id = index_i + index_j / SPLIT_MEM;
      int i = id >> log_column_count;
      int where = colt * row_count + i * POINTS;

      if (index_j % SPLIT_MEM == 0) {
        data = read_channel_intel(chan_mwt_out[1]);

        if (twiddle) {
          i = where & (row_count - 1);
          colt = where >> log_row_count;

          float2 twid_theta;
          float2 twid_delta;
          float cosine;

          // delta_const = -2.0f * (float)M_PI / (column_count * row_count) 
          float delta = colt * delta_const;
          if (inverse) {delta = -delta;}
          float theta = delta * (i + POINTS / 2);
        
          twid_theta.y = sincos(theta, &cosine);
          twid_theta.x = cosine;
        
          #if MIN_LOGNM >= 9    
            // delta is very close to 0. So can use Taylor approximation effectively.
            // For smaller LOGN, the SNR hit becomes noticeable so need to use proper
            // sincos() call.
            twid_delta.y = delta;                       // sin(delta) = delta
            twid_delta.x = 1.0f - (delta*delta) / 2.0f; // cos(delta) = 1 - delta^2/2
          #else
            // For smaller LOGN, the delta is not that close, so do sin and cos computations
            // properly.
            twid_delta.y = sincos(delta, &cosine);
            twid_delta.x = cosine;
          #endif

          // Compute sin/cos for a middle point exactly. Compute the rest incrementally by
          // adding/subtracting delta using these formulas
          //   sin(theta +/- delta) = sin(theta)*cos(delta) +/- sin(delta)*cos(theta)
          //   cos(theta +/- delta) = cos(theta)*cos(delta) -/+ sin(delta)*sin(theta)

          const int halfPoints = POINTS / 2;
          float2 twidFactors[POINTS + 1];
          twidFactors[halfPoints] = twidFactors[halfPoints + 1] = twid_theta;
        
        
          #pragma unroll
          for(int k = halfPoints - 1; k >= 0; --k) {
            twidFactors[k] = comp_mult(twidFactors[k + 1], comp_conj(twid_delta));
            data.d[k] = comp_mult(data.d[k], twidFactors[k]);
         }
         
          data.d[halfPoints] = comp_mult(data.d[halfPoints], twid_theta);
        
          #pragma unroll
          for(int k = halfPoints + 1; k < POINTS; ++k) {
            twidFactors[k] = comp_mult(twidFactors[k - 1], twid_delta);
            data.d[k] = comp_mult(data.d[k], twidFactors[k]);
          }
        } // end if (twiddle)
      }
    
      if(mangle) {
        where = mangle_bits(where, log_column_count + log_row_count);
      }

      #pragma unroll
      for (int ch = 0; ch < (POINTS / SPLIT_MEM); ch++) {
         dest[(where / POINTS) * POINTS + (index_j % SPLIT_MEM) * POINTS / SPLIT_MEM + ch] = (index_j % SPLIT_MEM == 1) ? data.d[POINTS / SPLIT_MEM + ch] : data.d[ch];
      }
    }
  }
}
