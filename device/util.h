// Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

#ifndef MANGLE_H
#define MANGLE_H

/* Accesses to DDR memory are efficient if the memory locations are accessed
 * in order. There is significant overhead when accesses are not in order. 
 * The penalty is higher if the accesses stride a large number of locations.
 *
 * This function provides the mapping for an alternative memory layout. This 
 * layout preserves some amount of linearity when accessing elements from the 
 * same matrix row, while bringing closer locations from the same matrix
 * column. The matrix offsets are represented using 2 * log(N) bits. This
 * function swaps bits log(N) - 1 ... log(N) / 2 with bits 
 * log(N) + log(N) / 2 - 1 ... log(N).
 *
 * The end result is that 2^(N/2) locations from the same row would still be 
 * consecutive in memory, while the distance between locations from the same 
 * column would be only 2^(N/2)
 */

uint mangle_bits(uint x, uint num_bits) {
  const uint NB = num_bits >> 2;
  uint a95 = x & (((1 << NB) - 1) << NB);
  uint a1410 = x & (((1 << NB) - 1) << (2 * NB));
  uint mask = ((1 << (2 * NB)) - 1) << NB;
  a95 = a95 << NB;
  a1410 = a1410 >> NB;
  return (x & ~mask) | a95 | a1410;
}
// Implements a complex number multiplication
TYPE comp_mult(TYPE a, TYPE b) {
  TYPE res;
  res.x = a.x * b.x - a.y * b.y;
  res.y = a.x * b.y + a.y * b.x;
  return res;
}

/* Complex conjugate of a */
float2 comp_conj(float2 a) {
  float2 result;
  result.x = a.x;
  result.y = -a.y;
  return result;
} 

/* Do (x >> s), where s is a variable that is smaller or equal to logN. */
uint var_rsh (uint x, uint s) {
  uint result = x;
  uchar s_small = s & MAX_LOGN_MASK;
  #pragma unroll
  for (uchar i=1; i<=LOGN; i++) {
    if (s_small >= i) result >>= 1;
  }
  return result;
}


#endif

