// Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

/* 
 * Multi-Wire Transpose.
 * This is a common core in-place transposition module for the kernels declared at the end.
 * Transposes N x POINTS rectangle of values. 
 * At each cycle, read/write POINTS samples, N times.
 * N can be changed dynamically, up to MAX_N.
 * POINTS must be set at compile time.
 */

#pragma OPENCL EXTENSION cl_intel_channels : enable
#include "../host/inc/fft_config.h"
#include "channels.h"

// Contains tables of permutations for FFT design.
// Original order of bits is 
//    [row][col][ch]
// where 'row' and 'ch' are both LOGPOINTS big, 'col' is LOGN - LOGPOINTS long
// 
// FETCH_TRANSPOSE:    [row][col][ch] --> [br(ch)][col][row]
// FETCH_NO_TRANSPOSE: [row][col][ch] --> [row][br(ch)][col]
// (below, 'col' is LOGN points long)
// TRANSPOSE:          [col[ch]       --> [ch][br(col)]
//

#include "perm.h"

// Permutation type naming scheme: "<FFT_KERNEL_NAME>[_<TRANSPOSE_OPTION>]"
// Note that fft contains a kernels named "fetch" and "transpose"
// Also, "fetch" kernel has a transpose option.
enum FFT_PERMUTATION_TYPE {
  PERMUTATION_TYPE_START = 0,
  FETCH_TRANSPOSE = PERMUTATION_TYPE_START,
  FETCH_NO_TRANSPOSE,
  TRANSPOSE,
  NUM_PERMUTATION_TYPES
};

// used for debugging only
#undef assert
#define assert(x)

// Permute bits of 'val' according to given permutation.
// If perm[]/perm_size are known at compile time, code in this
// function will be turned into wires.
uint do_perm2 (uint val, uchar perm_size, const PERM_TYPE perm[], uint logn) {
  uchar i;
  uint result = 0;
  #pragma unroll
  for (i=0; i < perm_size; i++) {
    uchar cur_bit = READ_BIT(val, i);
    result |= (cur_bit << (perm[i]));
  }
  // make sure neither input nor output had bits larger than LOGPOINTS+LOGN
  assert ( (val >> (logn+LOGPOINTS)) == 0 );
  assert ( (result >> (logn+LOGPOINTS)) == 0 );
  return result;
}


uint parity_with_mask (uint addr, const MASK_TYPE masks[LOGPOINTS], uint logn) {

  int ibit, ipt;
  uint result = 0;
  
  // do bit-wise xor of the result;
  #pragma unroll
  for (ipt = LOGPOINTS-1; ipt >= 0; ipt--) {
  
    uint masked = addr & masks[ipt];
    uint bit = 0;    
    #pragma unroll
    for (ibit = 0; ibit < LOGN+LOGPOINTS; ibit++) {
      bit ^= (masked & 0x1);
      masked >>= 1;
    }
    result = (result << 1) | bit;
  }
  assert ( (result >> (logn+LOGPOINTS)) == 0 );
  return result;
}


void get_row_col (uint tctr, uint sctr, uint igroup, 
                  uint *row, uint *col, 
                  uint logn, 
                  uint perm_type,
                  const MASK_TYPE perm1_masks[LOGPOINTS]) {
  
  uint ig;
  uint addr;
  uint addr_orig = (tctr << LOGPOINTS) | sctr;
  uint m = parity_with_mask (addr_orig, perm1_masks, logn);
 
  if (igroup == 0) {
    addr = addr_orig;
    *col = tctr;
  } else {
    
    uint m_t_parity = m ^ parity_with_mask (tctr << LOGPOINTS, perm1_masks, logn);
    addr = (tctr << LOGPOINTS) | m_t_parity;
    
    // do_perm() is defined in perm.h
    addr = do_perm (logn, LOGPOINTS, perm_type, igroup-1, addr);
    
    // DROP HIGHEST BIT OF EACH PERMUTATION.
    *col = addr >> LOGPOINTS; // drop lowest bits because now it contains the highest bits (after perm)
  }
  
  *row = m;  
  assert (*row < (1<<LOGPOINTS));
  assert (*col < (1<<logn));
}

// Keep pumping data, when ready, will print reordered version
void multiwire_transpose (uint log_column, uint log_row, enum FFT_PERMUTATION_TYPE type, int index) {

  uint logn = log_column;
  uint column_count = (1 << log_column);
  uint row_count = (1 << log_row);
  uint n = 1 << logn;

  #ifdef FIXED_N
    column_count = 1 << LOGN;
  #endif
  #ifdef FIXED_M
    row_count = 1 << LOGM;
  #endif

  // swap row and column so local memory banking works correctly
  // The attribute is added so this goes into on-chip RAM, even for small sizes
  // of N and POINTS.
  TYPE buf[N][POINTS] __attribute__((memory));
  int enable_reads;
  int enable_writes;
  int i_write, i_read;
  int group_write, group_read; // read transposed pattern of write.
  int group_read_bounded; // Optimize for loop recombine bound restriction
  int num_writes, num_reads;
  enum FFT_PERMUTATION_TYPE old_type = NUM_PERMUTATION_TYPES;
  int k, j, j_perm2;

  // reset all counters when type changes
  if (old_type != type) {
    old_type = type;
    enable_reads = 0;
    enable_writes = 1;
    i_write = 0;
    i_read = 0;
    group_write = 0;
    group_read = 1;
    group_read_bounded = 0;
    num_writes = 0;
    num_reads = 0;
  }
  
  // Load data for needed permutation from perm_data.c        
  PERM_TYPE perm2[LOGPOINTS];
  MASK_TYPE perm1_masks[LOGPOINTS];
  PERM_TYPE perm1_period   = perm_info_perm1_period[logn-perm_info_min_logn][LOGPOINTS-perm_info_min_logpoints][type];
  
  #pragma unroll
  for (k=0; k<LOGPOINTS; k++) {
    perm2[k]       = perm_info_perm2       [logn-perm_info_min_logn][LOGPOINTS-perm_info_min_logpoints][type][k];
    perm1_masks[k] = perm_info_perm1_masks [logn-perm_info_min_logn][LOGPOINTS-perm_info_min_logpoints][type][k];
  }

  for (k = 0; k < (column_count * row_count /POINTS) + column_count; k++) {

    if (k == (column_count * row_count/POINTS) ) {
      enable_writes = 0;
    }
      
    if (enable_reads) {
      // Mark st[] and r[] as registers so the compiler never puts them into
      // (stallable) local memory. Ideally, all these become wires.
      TYPE st[POINTS] __attribute__((register));
      short ri_col[POINTS] __attribute__((register));
      char  ri_idx[POINTS] __attribute__((register));

      group_read = group_read_bounded + 1;
      if (group_read == perm1_period) group_read = 0;
      
      #pragma unroll
      for (j = 0; j < POINTS; j++) {
        uint row, col;
        get_row_col (i_read, j, group_read, &row, &col, logn, type, perm1_masks);

        // record which col to access for each 1st buf dim, and which point index it gives you.
        // This way, can access 1st dim of buf with compile-time known index.
        ri_col[row] = col;
        ri_idx[row] = j;
      }
      #pragma unroll
      for (j = 0; j < POINTS; j++) {
        // read points out of order (but access buf[] in order!)
        assert (j < POINTS);
        assert (ri_col[j] < n);
        st[ ri_idx[j] ] = buf[ ri_col[j] ][j];
      }
      float_vector st2;
      #pragma unroll
      for (j = 0; j < POINTS; j++) 
        st2.d[j] = st[j];
        write_channel_intel (chan_mwt_out[index], st2);
      
      if (i_read == n-1) {
        if (group_read_bounded == perm1_period-1) group_read_bounded = 0;
        else group_read_bounded++;
        i_read = 0;
      }
      else {
        i_read++;
      }
    }
    
    if (enable_writes) {
      // reorder points according to parity of the address.
      // this creates an all-to-all cross bar for reorder[] 
      // but makes 1st dim of buf[] have one compile-time known
      // write and one compile-time known read.
      
      // now reorder based on parity among different memory banks
      int col_reorder[POINTS];
      TYPE reorder_write[POINTS] __attribute__((register));

      float_vector cd __attribute__((register));
      cd = read_channel_intel(chan_mwt_in[index]);
      #pragma unroll
      for (j_perm2 = 0; j_perm2 < POINTS; j_perm2++) {
        uint row, col;
         // TODO: Assuming that perm2 is its own inverse!
        uint j_perm2_inv = do_perm2 (j_perm2, LOGPOINTS, perm2, logn);
        get_row_col (i_write, j_perm2_inv, group_write, &row, &col, logn, type, perm1_masks);
        reorder_write[row] = cd.d[j_perm2];
        col_reorder[row] = col;
      }

      // write reordered points to ordered 1st buf dim
      #pragma unroll
      for (j = 0; j < POINTS; j++) {
        assert (j < POINTS);
        assert (col_reorder[j] < n);
        buf[ col_reorder[j] ][j] = reorder_write[j];
      }

      if (i_write == n-1) {
        // Enable reads on first full buffer
        enable_reads = 1;
        if (group_write == perm1_period-1) group_write = 0;
        else group_write++;
        i_write = 0;
      }
      else {
        i_write++;
      }
    }
  }
}


__attribute__((max_global_work_dim(0)))
kernel void fetch_MWT(uint log_column, uint log_row, int transpose) {
   multiwire_transpose(log_column, log_row, transpose? FETCH_TRANSPOSE : FETCH_NO_TRANSPOSE, 0);
}


__attribute__((max_global_work_dim(0)))
kernel void transpose_MWT(uint log_column, uint log_row) {
   multiwire_transpose(log_column, log_row, TRANSPOSE, 1);
}
