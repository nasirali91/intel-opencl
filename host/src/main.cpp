// Copyright (C) 2013-2019 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

///////////////////////////////////////////////////////////////////////////////////
// This OpenCL application executes a 1D FFT transform on an Intel FPGA.
// The kernel is defined in a device/fft.cl file.  The Intel 
// Offline Compiler tool ('aoc') compiles the kernel source into a 'fft.aocx' 
// file containing a hardware programming image for the FPGA.  The host program 
// provides the contents of the .aocx file to the clCreateProgramWithBinary OpenCL
// API for runtime programming of the FPGA.
//
// When compiling this application, ensure that the Intel(R) FPGA SDK for OpenCL(TM)
// is properly installed.
///////////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <cstdio>
#include <cstdlib>
#define _USE_MATH_DEFINES
#include <math.h>
#include <float.h>
#include <cstring>
#include <CL/opencl.h>
#include <CL/cl_ext_intelfpga.h>
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;

// FFT operates with complex numbers - store them in a struct
typedef struct {
  float x;
  float y;
} float2;

typedef struct {
  double x;
  double y;
} double2;

typedef struct {
  long x;
  long y;
  long z;
  long w;
} ulong4;

typedef struct {
  long x[8];
} ulong8;

typedef struct {
  long x[16];
} ulong16;

#include "fft_config.h"

// Check the status returned by the OpenCL API functions
#define CHECK(status) 								\
	if (status != CL_SUCCESS)						\
{									\
	fprintf(stderr, "error %d in line %d.\n", status, __LINE__);	\
	exit(1);							\
}									\

// Check the status returned by the OpenCL API functions, don't exit on error
#define CHECK_NO_EXIT(status) 								\
	if (status != CL_SUCCESS)						\
{									\
	fprintf(stderr, "error %d in line %d.\n", status, __LINE__);	\
}									\

// ACL runtime configuration
static cl_platform_id platform = NULL;
static cl_device_id device = NULL;
static cl_context context = NULL;

static cl_command_queue queue = NULL, queue2 = NULL, queue3 = NULL;
static cl_command_queue queue4 = NULL, queue5 = NULL;

static cl_kernel fft_kernel = NULL, fetch_kernel = NULL, transpose_kernel = NULL;
static cl_kernel fetch_MWT_kernel = NULL, transpose_MWT_kernel = NULL;

static cl_program program = NULL;
static cl_int status = 0;

// Function prototypes
bool init();
void init_dev_mem(int log_rows, int log_columns);
void cleanup();
static bool test_fft(bool mangle, bool inverse, int log_columns, int log_rows);
static int coord(int i, int j, int columns);
static void fourier_transform_gold(bool inverse, int lognr_points, double2 * data);
static void fourier_stage(int lognr_points, double2 * data);
static cl_uint mangle_bits(cl_uint x, cl_uint num_bits);

// Control whether the fast emulator should be used.
bool use_fast_emulator = false;

// Host memory buffers
float2 *h_inData, *h_outData, *h_tmp, *h_debug;
double2 *h_verify, *h_verify_tmp;

// Device memory buffers
cl_mem d_inData, d_outData, d_tmp;

// Entry point.
int main(int argc, char **argv) {
  Options options(argc, argv);

  // Optional argument to specify whether the fast emulator should be used.
  if(options.has("fast-emulator")) {
    use_fast_emulator = options.get<bool>("fast-emulator");
  }

  if(!init()) {
    return -1;
  }
  // Allocate host memory
  // N is defined in fft_config.h as (1 << LOGN)
  printf("LOGN = %d\n", LOGN);
  h_inData = (float2 *)alignedMalloc(sizeof(float2) * N * N);
  h_outData = (float2 *)alignedMalloc(sizeof(float2) * N * N);
  h_tmp = (float2 *)alignedMalloc(sizeof(float2) * N * N);
  h_verify = (double2 *)alignedMalloc(sizeof(double2) * N * N);
  h_verify_tmp = (double2 *)alignedMalloc(sizeof(double2) * N * N);
  if (!(h_inData && h_outData && h_verify && h_verify_tmp && h_tmp)) {
    printf("ERROR: Couldn't create host buffers\n");
    return -1;
  }

  // Allocate device memory.
  init_dev_mem(LOGN, LOGM);

  bool all_passed = true;
  if (options.has("mode")){
    bool inverse = false;

    if (options.get<std::string>("mode") == "normal") {
      inverse = false;
    }
    else if (options.get<std::string>("mode") == "inverse"){
      inverse = true;
    }

    const bool mangle = (LOGM>=8);
    all_passed &= test_fft(mangle, inverse, LOGN, LOGM);

    printf ("Test status: %s\n", all_passed ? "PASSED" : "FAILED");
  } else {
    // mangling is not supported (or needed) for very small sizes.
    // for logpoints=4, mangling only works for logn >= 8 (where logm<=logn)
    for (int inv = 0; inv < 2; inv++) {
      const bool mangle = (LOGM>=8);
      all_passed &= test_fft(mangle, inv, LOGN, LOGM);
    }
    
    printf ("Overall test status: %s\n", all_passed ? "ALL PASSED" : "SOME FAILED");
  }

  // Free the resources allocated
  cleanup();

  return 0;
}

bool test_fft(bool mangle, bool inverse, int log_rows, int log_columns) {

  int columns = (1 << log_columns);
  int rows = (1 << log_rows);

  printf("Launching %sFFT transform (%s data layout)\n", inverse ? "inverse " : "", mangle ? "alternative" : "ordered");
  printf("Number of points NxM = %d (%dx%d)\n", (rows * columns), rows, columns);
  
  
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < columns; j++) {
      int coor = coord(i, j, columns);
      int where = mangle ? mangle_bits(coor, log_rows+log_columns) : coor;
      h_verify[coor].x = h_inData[where].x = (float)i; 
      h_verify[coor].y = h_inData[where].y = (float)j;
    }
  }

  // Copy data from host to device
  status = clEnqueueWriteBuffer(queue, d_inData, CL_TRUE, 0, sizeof(float2) * columns * rows, h_inData, 0, NULL, NULL);
  CHECK(status);

  // Can't pass bool to device, so convert it to int
  int inverse_int = inverse;
  
  // Do this ones on host instead of every time on FPGA
  float delta_const = -2.0f * (float)M_PI / (columns * rows);
  
  // Can't pass bool to device, so convert it to int
  int mangle_int = mangle;
  // Can't pass bool to device, so convert it to int
  int twidle_int;
  printf("Kernel initialization is complete.\n");

  // Get the iterationstamp to evaluate performance
  double time = getCurrentTimestamp();
 
  // Loop twice over the kernels
  for (int i = 0; i < 2; i++) {

    cl_uint rows_arg, columns_arg, log_rows_arg, log_columns_arg;
    cl_uint work_group_size;
    // first iteration for fft and transpose run on transposed data.
    if (i == 0) {
      work_group_size = rows;
      rows_arg = rows;
      columns_arg = columns;
      log_rows_arg = log_rows;
      log_columns_arg = log_columns;
    } else {
      work_group_size = columns;
      rows_arg = columns;
      columns_arg = rows;
      log_rows_arg = log_columns;
      log_columns_arg = log_rows;
    }
    twidle_int = !i;
    
    // Set the kernel arguments
    status = clSetKernelArg(fetch_kernel, 0, sizeof(cl_mem), i == 0 ? (void *)&d_inData : (void *)&d_tmp);
    CHECK(status);
    status = clSetKernelArg(fetch_kernel, 1, sizeof(cl_int), (void*)&mangle_int);
    CHECK(status);
    status = clSetKernelArg(fetch_kernel, 2, sizeof(cl_int), (void*)&twidle_int);
    CHECK(status);
    status = clSetKernelArg(fetch_kernel, 3, sizeof(cl_int), (void*)&log_rows_arg);
    CHECK(status);
    status = clSetKernelArg(fetch_kernel, 4, sizeof(cl_int), (void*)&log_columns_arg);
    CHECK(status);
    status = clSetKernelArg(fetch_kernel, 5, sizeof(cl_int), (void*)&rows_arg);
    CHECK(status);
    status = clSetKernelArg(fetch_kernel, 6, sizeof(cl_int), (void*)&columns_arg);
    CHECK(status);
    status = clEnqueueTask(queue, fetch_kernel, 0, NULL, NULL);
    CHECK(status);
    
    
    status = clSetKernelArg(fetch_MWT_kernel, 0, sizeof(cl_int), (void*)&log_rows_arg);
    CHECK(status);
    status = clSetKernelArg(fetch_MWT_kernel, 1, sizeof(cl_int), (void*)&log_columns_arg);
    CHECK(status);
    status = clSetKernelArg(fetch_MWT_kernel, 2, sizeof(cl_int), (void*)&twidle_int);
    CHECK(status);
    status = clEnqueueTask(queue4, fetch_MWT_kernel, 0, NULL, NULL);
    CHECK(status);

    // Launch the fft kernel - we launch a single work item hence enqueue a task
    status = clSetKernelArg(fft_kernel, 0, sizeof(cl_int), (void*)&inverse_int);
    CHECK(status);
    status = clSetKernelArg(fft_kernel, 1, sizeof(cl_int), (void*)&log_rows_arg);
    CHECK(status);
    status = clSetKernelArg(fft_kernel, 2, sizeof(cl_int), (void*)&log_columns_arg);
    CHECK(status);
    status = clSetKernelArg(fft_kernel, 3, sizeof(cl_int), (void*)&rows_arg);
    CHECK(status);
    status = clSetKernelArg(fft_kernel, 4, sizeof(cl_int), (void*)&columns_arg);
    CHECK(status);
    status = clEnqueueTask(queue2, fft_kernel, 0, NULL, NULL);
    CHECK(status);

    status = clSetKernelArg(transpose_MWT_kernel, 0, sizeof(cl_int), (void*)&log_rows_arg);
    CHECK(status);
    status = clSetKernelArg(transpose_MWT_kernel, 1, sizeof(cl_int), (void*)&log_columns_arg);
    CHECK(status);
    status = clEnqueueTask(queue5, transpose_MWT_kernel, 0, NULL, NULL);
    CHECK(status);

    
    // Set the kernel arguments
    status = clSetKernelArg(transpose_kernel, 0, sizeof(cl_mem), i == 0 ? (void *)&d_tmp : (void *)&d_outData);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 1, sizeof(cl_int), (void*)&mangle_int);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 2, sizeof(cl_int), (void*)&twidle_int);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 3, sizeof(cl_int), (void*)&inverse_int);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 4, sizeof(cl_int), (void*)&log_rows_arg);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 5, sizeof(cl_int), (void*)&log_columns_arg);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 6, sizeof(cl_int), (void*)&rows_arg);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 7, sizeof(cl_int), (void*)&columns_arg);
    CHECK(status);
    status = clSetKernelArg(transpose_kernel, 8, sizeof(cl_float), (void*)&delta_const);
    CHECK(status);
	
    status = clEnqueueTask(queue3, transpose_kernel, 0, NULL, NULL);
    CHECK(status);

    // Wait for all command queues to complete pending events
    status = clFinish(queue);
    CHECK(status);
    status = clFinish(queue2);
    CHECK(status);
    status = clFinish(queue3);
    CHECK(status);
    status = clFinish(queue4);
    CHECK(status);
    status = clFinish(queue5);
    CHECK(status);
  }

  // Record execution time
  time = getCurrentTimestamp() - time;

  // Copy results from device to host
  status = clEnqueueReadBuffer(queue, d_outData, CL_TRUE, 0, sizeof(float2) * columns * rows, h_outData, 0, NULL, NULL);
  CHECK(status);
  printf("\tProcessing time = %.4fms\n", (float)(time * 1E3));
  double gpoints_per_sec = ((double) columns * rows / time) * 1E-9;
  double gflops = 5 * columns * rows * (log((float)columns * rows)/log((float)2))/(time * 1E9);
  printf("\tThroughput = %.4f Gpoints / sec (%.4f Gflops)\n", gpoints_per_sec, gflops);

  // Check signal to noise ratio
  fourier_transform_gold(inverse, log_columns + log_rows , h_verify);

  double mag_sum = 0;
  double noise_sum = DBL_MIN;
  
  // The output will be transposed
  for (int i = 0; i < columns; i++) {
    for (int j = 0; j < rows; j++) {
	  int coor = coord(i, j, rows);
      int where = mangle ? mangle_bits(coor, log_columns+log_rows) : coor;
      double2 v = h_verify[coor];
      float2 d = h_outData[where];
      
      double magnitude = v.x * v.x + v.y * v.y;
      double noise = (v.x - (double)d.x) * (v.x - (double)d.x) +  
                     (v.y - (double)d.y) * (v.y - (double)d.y);

      mag_sum += magnitude;
      noise_sum += noise;
    }
  }
  double db = 10 * log(mag_sum / noise_sum) / log(10.0);
  bool passed = db > 120;
  printf("\tSignal to noise ratio on output sample: %f --> %s\n\n", db, passed ? "PASSED" : "FAILED");
  
  return passed;
}


/////// HELPER FUNCTIONS ///////

// provides a linear offset in the input array
int coord(int i, int j, int columns) {
  return i * columns + j;
}

// This modifies the linear matrix access offsets to provide an alternative
// memory layout to improve the efficiency of the memory accesses
cl_uint mangle_bits(cl_uint x, cl_uint num_bits) {
  const cl_uint NB = num_bits >> 2;
  // aXY variable names refer to bits [X:Y]. Actual
  // numbers are chosen for LOGN=10 case
  cl_uint a95 = x & (((1 << NB) - 1) << NB);
  cl_uint a1410 = x & (((1 << NB) - 1) << (2 * NB));
  cl_uint mask = ((1 << (2 * NB)) - 1) << NB;
  a95 = a95 << NB;
  a1410 = a1410 >> NB;
  cl_uint result = (x & ~mask) | a95 | a1410;
  return result;
}


// Reference Fourier transform
void fourier_transform_gold(bool inverse, const int lognr_points, double2 *data) {
   const int nr_points = 1 << lognr_points;

   // The inverse requires swapping the real and imaginary component

   if (inverse) {
      for (int i = 0; i < nr_points; i++) {
         double tmp = data[i].x;
         data[i].x = data[i].y;
         data[i].y = tmp;
      }
   }
   // Do a FFT recursively
   fourier_stage(lognr_points, data);

   // The inverse requires swapping the real and imaginary component
   if (inverse) {
      for (int i = 0; i < nr_points; i++) {
         double tmp = data[i].x;
         data[i].x = data[i].y;
         data[i].y = tmp;
      }
   }
}

void fourier_stage(int lognr_points, double2 *data) {
   int nr_points = 1 << lognr_points;
   if (nr_points == 1) return;
   double2 *half1 = (double2 *)malloc(sizeof(double2) * nr_points / 2);
   double2 *half2 = (double2 *)malloc(sizeof(double2) * nr_points / 2);
   if (!(half1 && half2)) {
      printf("ERROR: Couldn't allocate memory for fourier_stage\n");
      exit(1);
   }
   for (int i = 0; i < nr_points / 2; i++) {
      half1[i] = data[2 * i];
      half2[i] = data[2 * i + 1];
   }
   fourier_stage(lognr_points - 1, half1);
   fourier_stage(lognr_points - 1, half2);
   for (int i = 0; i < nr_points / 2; i++) {
      data[i].x = half1[i].x + cos (2 * M_PI * i / nr_points) * half2[i].x + sin (2 * M_PI * i / nr_points) * half2[i].y;
      data[i].y = half1[i].y - sin (2 * M_PI * i / nr_points) * half2[i].x + cos (2 * M_PI * i / nr_points) * half2[i].y;
      data[i + nr_points / 2].x = half1[i].x - cos (2 * M_PI * i / nr_points) * half2[i].x - sin (2 * M_PI * i / nr_points) * half2[i].y;
      data[i + nr_points / 2].y = half1[i].y + sin (2 * M_PI * i / nr_points) * half2[i].x - cos (2 * M_PI * i / nr_points) * half2[i].y;
   }
   free(half1);
   free(half2);
}

// Create device buffers - potentially assign the buffers in different banks for more efficient
// memory access.
void init_dev_mem(int log_rows, int log_columns) {
  int columns = (1 << log_columns);
  int rows = (1 << log_rows);

  d_inData = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float2) * columns * rows, NULL, &status);
  CHECK(status);
#ifdef SEPARATE_OUT_BUF
  d_outData = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float2) * columns * rows, NULL, &status);
#else
  d_outData = d_inData;
#endif
  CHECK(status);
  d_tmp = clCreateBuffer(context, CL_MEM_READ_WRITE  | CL_CHANNEL_2_INTELFPGA , sizeof(float2) * columns * rows, NULL, &status);
  CHECK(status);

}

bool init() {
  cl_int status;

  // Get the OpenCL platform.
  if (use_fast_emulator) {
    platform = findPlatform("Intel(R) FPGA Emulation Platform for OpenCL(TM)");
  } else {
    platform = findPlatform("Intel(R) FPGA SDK for OpenCL(TM)");
  }
  if(platform == NULL) {
    printf("ERROR: Unable to find Intel FPGA OpenCL platform\n");
    return false;
  }

  // Query the available OpenCL devices.
  scoped_array<cl_device_id> devices;
  cl_uint num_devices;

  devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));

  // We'll just use the first device.
  device = devices[0];

  // Create the context.
  context = clCreateContext(NULL, 1, &device, NULL, NULL, &status);
  CHECK(status);

  // Create one command queue for each kernel.
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  CHECK(status);
  queue2 = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  CHECK(status);
  queue3 = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  CHECK(status);
  queue4 = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  CHECK(status);
  queue5 = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  CHECK(status);

  // Create the program.
  std::string binary_file = getBoardBinaryFile("bin/fft1d_offchip", device);
  printf("Using AOCX: %s\n\n", binary_file.c_str());
  program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);

  // Build the program that was just created.
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  CHECK(status);

  // Create the kernel - name passed in here must match kernel name in the
  // original CL file, that was compiled into an AOCX file using the AOC tool
  fft_kernel = clCreateKernel(program, "fft1d", &status);
  CHECK(status);
  fetch_kernel = clCreateKernel(program, "fetch", &status);
  CHECK(status);
  fetch_MWT_kernel = clCreateKernel(program, "fetch_MWT", &status);
  CHECK(status);
  transpose_kernel = clCreateKernel(program, "transpose", &status);
  CHECK(status);
  transpose_MWT_kernel = clCreateKernel(program, "transpose_MWT", &status);
  CHECK(status);

  return true;
}

// Free the resources allocated during initialization
void cleanup() {
  if(fft_kernel)
    clReleaseKernel(fft_kernel);
  if(fetch_kernel)
    clReleaseKernel(fetch_kernel);
  if(transpose_kernel)
    clReleaseKernel(transpose_kernel);
  if(fetch_MWT_kernel)
    clReleaseKernel(fetch_MWT_kernel);
  if(transpose_MWT_kernel)
    clReleaseKernel(transpose_MWT_kernel);
  if(program)
    clReleaseProgram(program);
  if(queue)
    clReleaseCommandQueue(queue);
  if(queue2)
    clReleaseCommandQueue(queue2);
  if(queue3)
    clReleaseCommandQueue(queue3);
  if(queue4)
    clReleaseCommandQueue(queue4);
  if(queue5)
    clReleaseCommandQueue(queue5);
  if(context)
    clReleaseContext(context);
  if(h_inData)
    alignedFree(h_inData);
  if (h_outData)
    alignedFree(h_outData);
 if (h_tmp)
    alignedFree(h_tmp);
  if (h_verify)
    alignedFree(h_verify);
  if (d_outData && (d_outData!=d_inData))
    clReleaseMemObject(d_outData);
  if (d_inData)
    clReleaseMemObject(d_inData);
}



